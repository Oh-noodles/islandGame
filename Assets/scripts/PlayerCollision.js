﻿#pragma strict

private var doorIsOpen:boolean = false;
private var doorTimer:float = 0.0;
private var currentDoor:GameObject;
var doorOpenAudio:AudioClip;
var doorCloseAudio:AudioClip;

private var collide:boolean = false;

var doorOpenTime:float = 3.0;

/* function Start () {
	
}
 */
function Update () {
	print("碰撞状态:"+collide);
	if (doorIsOpen)
	{
		doorTimer += Time.deltaTime;
		
		if (doorTimer > doorOpenTime)
		{
			CloseDoor(currentDoor);
			doorTimer = 0.0;
		}
	}
}

function OnCollisionEnter(hit:Collision) 
//function OnControllerColliderHit(hit:ControllerColliderHit)
{
	collide = true;
	if (hit.gameObject.tag == "PlayerAndDoor" && doorIsOpen == false )
	{
		currentDoor = hit.gameObject;
		OpenDoor(hit.gameObject);
	}
}

function OpenDoor(door:GameObject)
{
	doorIsOpen = true;
	door.GetComponent.<AudioSource>().PlayOneShot(doorOpenAudio);
	door.transform.parent.GetComponent.<Animation>().Play("openDoor");
}

function CloseDoor(door:GameObject)
{
	doorIsOpen = false;
	door.GetComponent.<AudioSource>().PlayOneShot(doorCloseAudio);
	door.transform.parent.GetComponent.<Animation>().Play("closeDoor");
}